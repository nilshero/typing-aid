*OUTDATED* This repository is outdated and i will not update it any more. Please use [this newer version](https://github.com/ManiacDC/TypingAid).

# Typing Aid
[AutoHotkey][ahk] script which adds configurable autocompletion to text editors on Windows.

Downloads: [sourcecode][down src], [standalone executable][down ex]

The [wiki] contains installing/usage instructions and more.

Contact: [Nils-Hero Lindemann]

[ahk]: http://ahkscript.org/ "AutoHotkey"
[down ex]: https://bitbucket.org/nilshero/typing-aid/downloads/Typing_Aid_2.19_standalone.zip "Download the Typing Aid standalone executable"
[down src]: https://bitbucket.org/nilshero/typing-aid/downloads/Typing_Aid_2.19_sourcecode.zip "Download the Typing Aid sourcecode"
[Nils-Hero Lindemann]: mailto:nilsherolindemann@gmail.com "nilsherolindemann@gmail.com"
[src]: https://bitbucket.org/nilshero/typing-aid/src "Typing Aid -> Sources"
[overview]: https://bitbucket.org/nilshero/typing-aid/overview "Typing Aid -> Overview"
[wiki]: https://bitbucket.org/nilshero/typing-aid/wiki/Home "Typing Aid wiki"
